public class TaskTwo {

    public static void main(String[] args) {
        int x = 100;
        System.out.println("Initial value of x: " + x);

        x += 6;
        System.out.println("After adding 6: " + x);

        x -= 4;
        System.out.println("After subtracting 4: " + x);

        x *= 4;
        System.out.println("After multiplying by 4: " + x);

        x /= 27;
        System.out.println("After dividing by 27: " + x);

        x %= 7;
        System.out.println("After modulus division by 7: " + x);
    }
}
